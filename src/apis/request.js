import axios from "axios";
import { message } from "antd";

const request = axios.create({
    baseURL: "http://localhost:8081/",
});

/* request.interceptors.response.use(
    (response) => response,
    (error) => {
        console.log(error.response.data);
        const msg = error.response.data?.msg;
        if(msg) {
            message.error(msg);
        }
        return Promise.reject(error);
    } 
)*/

export default request;