import request from "./request";

export const loadTodos = () =>{
    return request.get('/todos')
}

export const toggleTodo = (id, data) => {
    return request.put(`/todos/${id}`,{
        id: id,
        done: data.done,
        text: data.text,
    })
}

export const deleteTodo = (id) =>{
    return request.delete(`/todos/${id}`)
}

export const createTodo = (data) =>{
    return request.post(`/todos`,{
        text: data.text
    })
}

export const findTodoById = (id) => {
    return request.get(`/todos/${id}`)
}
