import { TodoItem } from "./TodoItem"

export function TodoGroup (props){
    const list = props.TodoList
    
    return <>
      <div>
          {list.map((item, index) => (
          <TodoItem key={index} item={item} />
        ))}
      </div>
    </>
}