import './Layout.css';
import { NavLink, Outlet } from "react-router-dom";

const Layout = () =>{
    return <div className="LayoutDiv">
        <nav>
        <ul>
          <li>
            <NavLink to="./about" activeclassname="activeLink">
              <span>AboutPage</span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/" activeclassname="activeLink">
              <span>Home</span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/done" activeclassname="activeLink">
              <span>DoneList</span>
            </NavLink>
          </li>
        </ul>
      </nav>

        <div className="contentWrapper">
        <Outlet />
        </div>
    </div>
}

export default Layout;