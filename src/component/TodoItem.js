import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { deleteTodo, toggleTodo, updateText } from "../apis/todo";
import { Button } from 'antd';
import { loadTodos } from "../apis/todo";
import { initTodos } from "../component/TodoSplice";
import './Todo.css';
import { useState } from "react";

export function TodoItem(props) {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [showModal, setShowModal] = useState(false); // 控制弹框的显示和隐藏
  const [text, setText] = useState('点击进行编辑'); // 初始文本

  async function updateDoneState(param) {
    const id = props.item.id;
    if (props.doUpdate) {
      navigate(`/todo/${id}`)
    } else if (param === "updateStatus") {
      await toggleTodo(props.item.id, {
        id: id,
        done: !props.item.done,
        text: props.item.text,
      })
    } else if (param === "updateText") {
      await toggleTodo(props.item.id, {
        id: id,
        done: props.item.done,
        text: text,
      })
      setShowModal(false)
    }
    loadTodos().then((response) => {
      dispatch(initTodos(response.data))
    })

  }

  async function deleteDoneState() {
    const id = props.item.id;
    await deleteTodo(id)
    loadTodos().then((response) => {
      dispatch(initTodos(response.data))
    })
  }

  function chanelUpdate(){
    setShowModal(false)
  }


  return <>
    <div className="container" style={{ border: props.item.done ? 'dashed' : 'solid' }}>
      {showModal ? <input value={text} onChange={event => setText(event.target.value)}></input> :
        <div className="box" style={{ textDecoration: props.item.done ? 'line-through' : 'none' }} onClick={() => updateDoneState("updateStatus")}>
          {props.item.text}
        </div>}
      {showModal ? <div><Button className="button" type="primary" onClick={() => updateDoneState("updateText")}>完成</Button> 
      <Button className="button" type="primary" onClick={chanelUpdate}>取消</Button></div> :
       <div> <Button className="button" type="primary" onClick={() => { setShowModal(true) }}>编辑</Button>  
       <Button className="button" type="primary" onClick={deleteDoneState}>删除</Button> </div>}
     
    </div>
  </>
}