import { createSlice } from "@reduxjs/toolkit";


const TodoSplice = createSlice({
    name: "todoList",
    initialState:{
        todoList:[]
    },
    reducers:{
        initTodos: (state, action) => {
            state.todoList = action.payload
        }
    },
});

export const {initTodos} = TodoSplice.actions;

export default TodoSplice.reducer;