import { nanoid } from "@reduxjs/toolkit";
import { useDispatch } from "react-redux";
import { createTodo } from "../apis/todo";
import { Button } from 'antd';
import { loadTodos } from "../apis/todo";
import { initTodos } from "../component/TodoSplice";

export function TodoGender (props) {
    const dispatch = useDispatch();

    async function handleButtonChange(e) {
        props.onInputChange('')
        await createTodo({
            text: props.Input
        })
        loadTodos().then((response) => {
            dispatch(initTodos(response.data))
        })
    }

    function handleInputChange(e){
        props.onInputChange(e.target.value)
    }

    return (
        <div> 
            <input value={props.Input || ''} onChange = {handleInputChange} />
            <Button type="primary" onClick={handleButtonChange}>点击</Button>
        </div>
        )
}