import { TodoGroup } from "./TodoGroup"
import { useState, useEffect } from 'react';
import { TodoGender } from "./TodoGender";
import { useSelector,useDispatch } from "react-redux";
import { loadTodos } from "../apis/todo";
import {initTodos} from "../component/TodoSplice"
import { importLink } from 'react-router-dom';
import './Todo.css'


export function TodoList () {
    const todoListNew = useSelector(state => state.todoList.todoList);
    const [Input, setInput] = useState()
    const dispatch = useDispatch()
    const handleInputChange = (Input) => {
        setInput(Input)
    }


    useEffect(() => {
        loadTodos().then((response) => {
            dispatch(initTodos(response.data))
        })
    },[])

    return(
        <div className="TodoList">
        <TodoGroup TodoList={todoListNew} />
        <TodoGender Input={Input} onInputChange={handleInputChange} />
        </div>
    )     
}