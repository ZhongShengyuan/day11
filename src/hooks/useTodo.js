import { useDispatch } from "react-redux"
import { initTodos } from "../component/TodoSplice"
import { loadTodos } from "../apis/todo";

const useTodo = () => {
    const dispatch =  useDispatch();

    const reloadTodos = () => {
        loadTodos().then((response) => {
            useDispatch(initTodos(response.data))
        })
    }

    return{
        reloadTodos,
    }
}