import { configureStore } from '@reduxjs/toolkit'
import TodoSplice from "../component/TodoSplice"

export default configureStore({
  reducer: {
    todoList: TodoSplice,
  },
});