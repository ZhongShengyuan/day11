import { useSelector } from "react-redux";
import { TodoItem } from "./component/TodoItem";
import { findTodoById } from './apis/todo';

const DoneList =() => {
    const todoListNew = useSelector(state => state.todoList.todoList.filter((todo) => {
        return todo.done;
    }));

    return(
        <div>
            {todoListNew.map((item, index) => (
            <TodoItem key={index} item={item} doUpdate="false"/>
            ))}
        </div>
    )
}

export default DoneList;