import { useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { findTodoById } from "../../apis/todo";

const TodoDetail = () => {
    const params = useParams();
    const { id } = params;
    const navigate = useNavigate();
    const [doneDetail, setDoneDetail] = useState({});

    useEffect(() => {
        const fetchData = () => {
            try {
                findTodoById(id).then(res => {
                    setDoneDetail(res.data);
                })
            } catch (error) {
                navigate("/404");
            }
        };
        fetchData();
    }, [id]);

    return (
        <>
            {doneDetail && (
                <div>
                    <p>id: {doneDetail.id}</p>
                    <p>content: {doneDetail.text}</p>
                    <p>done: {doneDetail.done ? "已完成" : "未完成"}</p>
                </div>
            )}
        </>
    );
};

export default TodoDetail;
