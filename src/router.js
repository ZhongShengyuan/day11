import * as React from "react";
import { createBrowserRouter, Link } from "react-router-dom";
import { TodoList } from "./component/TodoList";
import AboutPage from './page/About';
import Layout from './component/Layout';
import DoneList from './DoneList';
import TodoDetail from "./page/todo/TodoDetail"
import NotFoundPage from './page/NotFoundPage';

export const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children:[
      {
        index: true,
        element: <TodoList />,
      },
      {
        path: "/about",
        element: <AboutPage />,
      },
      {
        path: "/done",
        element: <DoneList />,
      },
      {
        path: "todo/:id",
        element: <TodoDetail />
      },
      {
        path: '*',
        element: <NotFoundPage />
      },
    ]
  },  
]);
